#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

int main()
{
    int tamY = 23;
    int tamX = 40;
    int x, y, yi;
    char world[tamY][tamX];
    char nome[50];
    char jogador = 'A';
    char jogadorLaser = '^';
    char inimigo = 'M';
    char inimigoescudo = 'O';
    char inimigolaser = 'l';
    char explosao = 'X';
    int score = 0;
    int victory = 1;
    int laserpronto = 1;
    int inimigopronto = 0;

    srand(time(NULL));

    /*ttela de inicil*/
    printf("\n \n    Bem vindo ao jogo com melhor interface grafica! \n \n \n \n");
    Sleep(200);
    printf("Digite seu nome : \n \n \n \n");
       gets(nome);
    Sleep(200);
    printf("Bem vindo PILOTO(A): %s", nome);
    Sleep(250);
    printf(" \nespero que esteja pronto para iniciar sua jornada  \n \n \n \n");
    Sleep(250);
    printf(" \nBoa viagem                ");
    Sleep(200);
    printf("\n \n \n \n digite qualquer tecla para continuar .");
    getch();


    /*iniciando */
    int totalInimigos = 0;
    for (x = 0; x < tamX; x ++) {
        for (y = 0; y < tamY; y ++) {
            if ((y+1) % 2 == 0 && y < 7 && x > 4
            && x < tamX - 5 && x % 2 ==0) {
                world[y][x] = inimigo;
                totalInimigos ++;
            }
            else if ((y+1) % 2 == 0 && y >= 7 && y < 9 && x > 4
            && x < tamX - 5 && x % 2 ==0){
                world[y][x] = inimigoescudo;
                totalInimigos = totalInimigos + 2;
            }
            else {
                world[y][x] = ' ';
            }
        }
    }
    world[tamY - 1][tamX / 2] = jogador;
    int i = 1;
    char direcao = 'l';
    char keyPress;
    int inimigosAtuais = totalInimigos;
    while(inimigosAtuais > 0 && victory) {
        int drop = 0;
        int velocidadeInimigo = 1 + 10 * inimigosAtuais / totalInimigos;
        laserpronto ++;

        /*mundo*/
        system("cls");
        printf("     SCORE:    %d", score);
        printf("\n");
            for (y = 0; y < tamY; y ++) {
            printf("|");
                for (x = 0; x < tamX; x ++) {
                    printf("%c",world[y][x]);
                }
            printf("|");
            printf("\n");
            }

        /*laser */
        for (x = 0; x < tamX; x ++) {
            for (y = tamY-1; y >= 0; y --) {
                if (i%2 == 0 && world[y][x] == inimigolaser
                && (world[y+1][x] != inimigo & world[y+1][x] != inimigoescudo)){
                world[y+1][x] = inimigolaser;
                world[y][x] = ' ';
                }
                else if (i%2 == 0 && world[y][x] == inimigolaser
                && (world[y+1][x] == inimigo | world[y+1][x] == inimigoescudo)){
                    world[y][x] = ' ';
                }
            }
        }
        for (x = 0; x < tamX; x ++) {
            for (y = 0; y < tamY; y ++) {
                if ((i % 5) == 0 && (world[y][x] == inimigoescudo
                | world[y][x] == inimigo) && (rand() % 15) > 13
                && world[y+1][x] != jogadorLaser) {
                    for (yi = y+1; yi < tamY; yi ++) {
                        if (world[yi][x] == inimigo
                        | world[yi][x] == inimigoescudo) {
                            inimigopronto = 0;
                            break;
                        }
                        inimigopronto = 1;
                    }
                    if (inimigopronto) {
                        world[y+1][x] = inimigolaser;
                    }
                }
                if (world[y][x] == jogadorLaser && world[y-1][x] == inimigo) {
                    world[y][x] = ' ';
                    world[y-1][x] = explosao;
                    inimigosAtuais --;
                    score = score + 50;
                }
                else if (world[y][x] == jogadorLaser
                && world[y-1][x] == inimigoescudo) {
                    world[y][x] = ' ';
                    world[y-1][x] = inimigo;
                    inimigosAtuais --;
                    score = score + 50;
                }
                else if (world[y][x] == jogadorLaser
                && world[y-1][x] == inimigolaser) {
                    world[y][x] = ' ';
                }
                else if (world[y][x] == explosao) {
                    world[y][x] = ' ';
                }
                else if ((i+1) % 2 == 0 && world[y][x] == inimigolaser
                && world[y+1][x] == jogador) {
                    world[y+1][x] = explosao;
                    world[y][x] = ' ';
                    victory = 0;
                }
                else if (world[y][x] == jogadorLaser
                && world[y-1][x] != inimigolaser) {
                        world[y][x] = ' ';
                        world[y-1][x] = jogadorLaser;
                }
            }
        }

        /*dire�ao inimigo*/
        for (y = 0; y < tamY; y ++) {
            if (world[y][0] == inimigo) {
                direcao = 'r';
                drop = 1;
                break;
            }
            if (world[y][tamX-1] == inimigo){
                direcao = 'l';
                drop = 1;
                break;
            }
        }

        /*atualiz�ao*/
        if (i % velocidadeInimigo == 0) {
            if (direcao == 'l') {
                for (x = 0; x < tamX - 1; x ++) {
                    for (y = 0; y < tamY; y ++) {
                        if (drop && (world[y-1][x+1] == inimigo
                            || world[y-1][x+1] == inimigoescudo)){
                            world[y][x] = world[y-1][x+1];
                            world[y-1][x+1] = ' ';
                        }
                        else if (!drop && (world[y][x+1] == inimigo
                            || world[y][x+1] == inimigoescudo)) {
                            world[y][x] = world[y][x+1];
                            world[y][x+1] = ' ';
                        }
                    }
                }
            }
            else {
                for (x = tamX; x > 0; x --) {
                    for (y = 0; y < tamY; y ++) {
                        if (drop && (world[y-1][x-1] == inimigo
                            || world[y-1][x-1] == inimigoescudo)) {
                            world[y][x] = world[y-1][x-1];
                            world[y-1][x-1] = ' ';
                        }
                        else if (!drop && (world[y][x-1] == inimigo
                            || world[y][x-1] == inimigoescudo)) {
                            world[y][x] = world[y][x-1];
                            world[y][x-1] = ' ';
                        }
                    }
                }
            }
            for (x = 0; x < tamX; x ++) {
                if (world[tamY - 1][x] == inimigo) {
                    victory = 0;
                }
            }
        }

        /*controle*/
        if(kbhit()){
            keyPress = getch();
        }
        else {
            keyPress = ' ';
        }
        if (keyPress == 'a') {
            for (x = 0; x < tamX; x = x+1) {
                if ( world[tamY-1][x+1] == jogador) {
                    world[tamY-1][x] = jogador;
                    world[tamY-1][x+1] = ' ';
                }
            }
        }

        if (keyPress == 'd') {
            for (x = tamX - 1; x > 0; x = x-1) {
                if ( world[tamY-1][x-1] == jogador) {
                    world[tamY-1][x] = jogador;
                    world[tamY-1][x-1] = ' ';
                }
            }
        }
        if (keyPress == 'm' && laserpronto > 2) {
            for (x = 0; x < tamX; x = x+1) {
                if ( world[tamY-1][x] == jogador) {
                    world[tamY - 2][x] = jogadorLaser;
                    laserpronto = 0;
                }
            }
        }
        i ++;
        Sleep(30);
    }
    system("cls");
        printf("     SCORE:    %d", score);
        printf("\n");
            for (y = 0; y < tamY; y ++) {
            printf("|");
                for (x = 0; x < tamX; x ++) {
                    printf("%c",world[y][x]);
                }
            printf("|");
            printf("\n");
            }
    Sleep(1000);
    system("cls");
    if (victory != 0) {
        printf("\n \n \n \n \n \n               Parabens \n \n \n \n \n");
        Sleep(1000);
        printf("\n \n               Score: %d", score);
        Sleep(1000);
        int bonus = totalInimigos*20 - i;
        printf("\n \n               Bonus: %d", bonus);
        Sleep(1000);
        printf("\n \n               Total Score: %d", score + bonus);
        printf("\n \n \n \n               Well done");
        Sleep(1000);
        printf(", Hero.");
        Sleep(1000);
        getch();
    }
    else {
        printf("\n \n \n \n \n \n ");
        printf(nome);
        printf(", You have failed with this Ship.");
        Sleep(1000);
        printf("\n \n \n \n \n \n               Voce morreu .");
        Sleep(1000);
        printf("\n \n               Final Score: %d", score);
        getch();
    }
}























